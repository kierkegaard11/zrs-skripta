#!/bin/bash

dnf -y install dnf-plugins-core

dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
    
dnf install docker-ce docker-ce-cli containerd.io

systemctl start docker

docker run -d --name mysqldemo -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql:latest

docker ps

rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022

dnf install git

wget https://mirror.umd.edu/eclipse/oomph/epp/2021-12/R/eclipse-inst-jre-linux64.tar.gz

tar -xzvf eclipse-inst-jre-linux64.tar.gz
cd eclipse-installer/
./eclipse-inst


